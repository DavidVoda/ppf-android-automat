# PPF Android Automated Tests

This repository contains the automated test suite for PPF CZ - android version. The iOS version has it's own separate repository.


## Requirements

- [Appium desktop client](https://github.com/appium/appium-desktop/releases/tag/v1.21.0)
- Maven, jdk and other standard setup
- [Android Studio](https://developer.android.com/studio) (optional, but recommended for emulators)

## General info

The actual test cases are specified in gherkin feature files (/bsc/features/). These cases contain isolated test scenarios for the given part of PPF application.

Each scenario comprises of atomic test steps, which are often available for re-use. For example:

> Then Tap on "NAVBAR_PAYMENTS"

This step calls the tapOn method, with NAVBAR_PAYMENTS being used as an argument for elementName. You can re-use this method for tapping on 99% of elements in the application by simply addind it's ID into the cloud datatable and then passing it to the method.

The datatable is stored on the company google drive, to make sure every instance of automated test suite has up-to-date dataset. To get access to it, please contact Roman Stepka. If needed, the source table can be change in googleDriveLink (TableReader class)

## Selecting which TCs to run

You can specify which TCs to run in the TestRunner class by adding or commenting out the feature files.

During development or debugging you can easily run a single TC by simply right clicking the feature in IntelliJ and hitting Run.

## Specifying the test device

You can specify the device on which you want to run the tests within the Constants class.
These constants are then used in the RuntimeSetup class (startServer method)

It is suggested to use an emulator (using Android Studio), or alternatively you can use a physical android device. In this case just make sure that the device has usb debugging allowed.
BitBar cloud farm is not usable on PPF project, since our plan doesn't support VPN access. If this changes in the future however, just add the cloud device info like you would with a local one.

Constants class should always be .gitignored (different testers use different setups)

## Running the test

First make sure that the Appium local server is up and running, and that your device is connected and unlocked.

Then you can simply run the test using maven:

> mvn test

Or during development, you can select the TestRunner class and hit run.

## Test reports

To use the allure webserver use the following steps:
Using cmd, navigate to \allure-2.9.0\bin
> - allure generate --clean
> - allure serve (your local path to the repo)/target/allure-results

This will start a local instance of Allure and present you with the test results in a very detailed and comprehensive way.
Alternatively, you can view the raw test results in the console.

Also there's a very primitive test report automatically generated in /target/reportz after each test run

## Useful resources

[Cucumber tutorial](https://cucumber.io/docs/guides/10-minute-tutorial/)

[Android emulator setup](https://developer.android.com/studio/run/emulator)

[BitBar cloud farm](https://bitbar.com/)

[How to use Allure test report](https://docs.qameta.io/allure/)

[]()

[]()

[]()


