package bsc;

import cucumber.api.CucumberOptions;
import cucumber.api.junit.Cucumber;
import org.junit.runner.RunWith;

/**
 * Test runner for cucumber.
 */
@RunWith(Cucumber.class)
@CucumberOptions(
        features = {
                //"src/test/java/bsc/features/001_smoke.feature",
                "src/test/java/bsc/features/105_payment_domestic.feature",
                //"src/test/java/bsc/features/102_internalTransfer.feature",
                //"src/test/java/bsc/features/103_currencyExchange.feature",
                //"src/test/java/bsc/features/105_mobileTopUp.feature",
                //"src/test/java/bsc/features/203_currentAccountProduct.feature",
                //"src/test/java/bsc/features/801_biometrics.feature",
                //"src/test/java/bsc/features/400_messageFlow.feature",
        },
        glue = {"bsc"},
        plugin = {"pretty", "html:target/reportz", "io.qameta.allure.cucumberjvm.AllureCucumberJvm"},
        monochrome = true)

public class TestRunner {
}
