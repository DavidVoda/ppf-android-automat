Feature: Payment domestic

#  Domestic payment, BBAN format + payment Sign
  Scenario: Domestic payment, BBAN format + payment Sign

    Given Appium server is running
    When Login is displayed

    When User is logged in

    When Tap on "NAVBAR_ACTION"

#   First step, debit account selection
    Then Tap on "PAYMENT_SELECT_ACCOUNT_FIRST"

#   Second step, credit account, bank and currency
    When Element "PAYMENT_TITLE" is displayed
    Then Tap on "PAYMENT_OPTION_MANUALLY"
    Then Enter text "123/0300" into "PAYMENT_ACCOUNT_NUMBER_INPUT"
    Then Enter text "100" into "PAYMENT_AMOUNT_INPUT"
    Then Tap on "PAYMENT_CURRENCY_INPUT"
    Then Tap on "PAYMENT_CURRENCY_FIRST"
    Then Tap on "PAYMENT_CONFIRM_BUTTON"

#   Third step, additional payment information with max values
    When Element "PAYMENT_DETAIL_TITLE" is displayed
    Then Enter text "Automation test Domestic BBAN SIGN" into "PAYMENT_PARTNER_ACCOUNT_NAME_INPUT"
    Then Enter text "1111222233" into "PAYMENT_VARIABLE_SYMBOL_INPUT"
    Then Enter text "6666777788" into "PAYMENT_SPECIFIC_SYMBOL_INPUT";
    Then Enter text "4444" into "PAYMENT_CONSTANT_SYMBOL_INPUT"
    Then Enter text "ABCDE12345ABCDE12345ABCDE12345ABCDE12345ABCDE12345ABCDE12345ABCDE12345ABCDE12345ABCDE12345ABCDE12345ABCDE12345ABCDE12345ABCDE12345ABCDE12345" into "PAYMENT_MESSAGE_BENEFICIARY_INPUT"
    Then Enter text "ABCDE12345ABCDE12345ABCDE12345ABCDE12345ABCDE12345" into "PAYMENT_MY_NOTES_INPUT"
    Then Tap on "PAYMENT_CONFIRM_BUTTON"

#   Sign payment
    When Element "PAYMENT_DETAIL_TITLE" is displayed
    Then Certificate by SMS

#   Double back to dashboard and logout
    Then Tap on "BACK_BUTTON"
    Then Tap on "BACK_BUTTON"


#  Domestic payment, IBAN format + payment Edit
  Scenario: Domestic payment, IBAN format + payment Edit

    When Tap on "NAVBAR_ACTION"

#   First step, debit account selection
    Then Tap on "PAYMENT_SELECT_ACCOUNT_FIRST"

#   Second step, credit account, bank and currency
    When Element "PAYMENT_TITLE" is displayed
    Then Tap on "PAYMENT_OPTION_MANUALLY"
    Then Enter text "CZ25 0300 0000 0000 0000 0123" into "PAYMENT_ACCOUNT_NUMBER_INPUT"
    Then Enter text "100" into "PAYMENT_AMOUNT_INPUT"
    Then Tap on "PAYMENT_CURRENCY_INPUT"
    Then Tap on "PAYMENT_CURRENCY_FIRST"
    Then Tap on "PAYMENT_CONFIRM_BUTTON"

#   Third step, additional payment information
    When Element "PAYMENT_DETAIL_TITLE" is displayed
    Then Enter text "Automation test Domestic IBAN" into "PAYMENT_PARTNER_ACCOUNT_NAME_INPUT"
    Then Enter text "1111222233" into "PAYMENT_VARIABLE_SYMBOL_INPUT"
    Then Enter text "6666777788" into "PAYMENT_SPECIFIC_SYMBOL_INPUT";
    Then Enter text "4444" into "PAYMENT_CONSTANT_SYMBOL_INPUT"
    Then Enter text "4444" into "PAYMENT_MESSAGE_BENEFICIARY_INPUT"
    Then Enter text "4444" into "PAYMENT_MY_NOTES_INPUT"
    Then Tap on "PAYMENT_CONFIRM_BUTTON"

#   Edit payment
    When Element "PAYMENT_DETAIL_TITLE" is displayed
    Then Tap on "PAYMENT_EDIT_BUTTON"
    Then Enter text "Automation test IBAN EDIT" into "PAYMENT_PARTNER_ACCOUNT_NAME_INPUT"
    Then Tap on "PAYMENT_PRIORITY_INPUT"
    Then Tap on "PAYMENT_PRIORITY_EXPRESS"
#   When payment is being created after 12:00 warning messages pops up and needs to be confirmed
    When Express payment is after noon
    Then Tap on "PAYMENT_CONFIRM_BUTTON"

#   Double back to dashboard
    Then Tap on "BACK_BUTTON"
    Then Tap on "BACK_BUTTON"

#  Domestic payment, BBAN format, PPF credit account + payment Delete
  Scenario: Domestic payment, BBAN format, PPF credit account + payment Delete

    When Tap on "NAVBAR_ACTION"

#   First step, debit account selection
    Then Tap on "PAYMENT_SELECT_ACCOUNT_FIRST"

#   Second step, credit account, bank and currency
    When Element "PAYMENT_TITLE" is displayed
    Then Tap on "PAYMENT_OPTION_MANUALLY"
    Then Enter text "2012740048/6000" into "PAYMENT_ACCOUNT_NUMBER_INPUT"
    Then Enter text "100" into "PAYMENT_AMOUNT_INPUT"
    Then Tap on "PAYMENT_CURRENCY_INPUT"
    Then Tap on "PAYMENT_CURRENCY_FIRST"
    Then Tap on "PAYMENT_CONFIRM_BUTTON"

#   Third step, additional payment information
    When Element "PAYMENT_DETAIL_TITLE" is displayed
    Then Enter text "Automation test Domestic IBAN" into "PAYMENT_PARTNER_ACCOUNT_NAME_INPUT"
    Then Enter text "1111222233" into "PAYMENT_VARIABLE_SYMBOL_INPUT"
    Then Enter text "6666777788" into "PAYMENT_SPECIFIC_SYMBOL_INPUT";
    Then Enter text "4444" into "PAYMENT_CONSTANT_SYMBOL_INPUT"
    Then Enter text "4444" into "PAYMENT_MESSAGE_BENEFICIARY_INPUT"
    Then Enter text "4444" into "PAYMENT_MY_NOTES_INPUT"
    Then Tap on "PAYMENT_CONFIRM_BUTTON"

#   Delete payment
    When Element "PAYMENT_DETAIL_TITLE" is displayed
    Then Tap on "PAYMENT_DELETE_BUTTON"
    Then Tap on "WIDGET_CONFIRM_BUTTON"

#   Back to dashboard
    Then Tap on "BACK_BUTTON"

#  Domestic payment, IBAN format, PPF credit account
  Scenario: Domestic payment, IBAN format, PPF credit account

    When Tap on "NAVBAR_ACTION"

#   First step, debit account selection
    Then Tap on "PAYMENT_SELECT_ACCOUNT_FIRST"

#   Second step, credit account, bank and currency
    When Element "PAYMENT_TITLE" is displayed
    Then Tap on "PAYMENT_OPTION_MANUALLY"
    Then Enter text "2012740048/6000" into "PAYMENT_ACCOUNT_NUMBER_INPUT"
    Then Enter text "100" into "PAYMENT_AMOUNT_INPUT"
    Then Tap on "PAYMENT_CURRENCY_INPUT"
    Then Tap on "PAYMENT_CURRENCY_FIRST"
    Then Tap on "PAYMENT_CONFIRM_BUTTON"

#   Third step, additional payment information
    When Element "PAYMENT_DETAIL_TITLE" is displayed
    Then Enter text "Automation test Domestic IBAN" into "PAYMENT_PARTNER_ACCOUNT_NAME_INPUT"
    Then Tap on "PAYMENT_PRIORITY_INPUT"
    Then Tap on "PAYMENT_PRIORITY_INSTANT"
    Then Enter text "1111222233" into "PAYMENT_VARIABLE_SYMBOL_INPUT"
    Then Enter text "6666777788" into "PAYMENT_SPECIFIC_SYMBOL_INPUT";
    Then Enter text "4444" into "PAYMENT_CONSTANT_SYMBOL_INPUT"
    Then Enter text "4444" into "PAYMENT_MESSAGE_BENEFICIARY_INPUT"
    Then Enter text "4444" into "PAYMENT_MY_NOTES_INPUT"
    Then Tap on "PAYMENT_CONFIRM_BUTTON"

#   Double back to dashboard
    Then Tap on "BACK_BUTTON"
    Then Tap on "BACK_BUTTON"
    Then Tap on "BACK_BUTTON"

#  Domestic payment, Instant priority check
  Scenario: Domestic payment, Instant priority

    When Tap on "NAVBAR_ACTION"

#   First step, debit account selection
    Then Tap on "PAYMENT_SELECT_ACCOUNT_FIRST"

#   Second step, credit account, bank and currency
    When Element "PAYMENT_TITLE" is displayed
    Then Tap on "PAYMENT_OPTION_MANUALLY"
    Then Enter text "1919/0800" into "PAYMENT_ACCOUNT_NUMBER_INPUT"
    Then Enter text "100" into "PAYMENT_AMOUNT_INPUT"
    Then Tap on "PAYMENT_CURRENCY_INPUT"
    Then Tap on "PAYMENT_CURRENCY_FIRST"
    Then Tap on "PAYMENT_CONFIRM_BUTTON"

#   Third step, additional payment information
    When Element "PAYMENT_DETAIL_TITLE" is displayed
    Then Enter text "Automation test Domestic IBAN" into "PAYMENT_PARTNER_ACCOUNT_NAME_INPUT"
    Then Tap on "PAYMENT_PRIORITY_INPUT"
    Then Tap on "PAYMENT_PRIORITY_INSTANT"
    Then Enter text "1111222233" into "PAYMENT_VARIABLE_SYMBOL_INPUT"
    Then Enter text "6666777788" into "PAYMENT_SPECIFIC_SYMBOL_INPUT";
    Then Enter text "4444" into "PAYMENT_CONSTANT_SYMBOL_INPUT"
    Then Enter text "4444" into "PAYMENT_MESSAGE_BENEFICIARY_INPUT"
    Then Enter text "4444" into "PAYMENT_MY_NOTES_INPUT"
    Then Tap on "PAYMENT_CONFIRM_BUTTON"

#   Edit payment for instant amount check
    When Element "PAYMENT_DETAIL_TITLE" is displayed
    Then Tap on "PAYMENT_EDIT_BUTTON"
    Then Enter text "400000" into "PAYMENT_AMOUNT_INPUT_EDIT"
    Then Tap on "PAYMENT_CONFIRM_BUTTON"
    Then Element "PAYMENT_ERROR_LABEL" is displayed

#   Cancel payment edit
    Then Tap on "BACK_BUTTON"
    Then Tap on "WIDGET_CONFIRM_BUTTON"

#   Double back to dashboard
    Then Tap on "BACK_BUTTON"
    Then Tap on "BACK_BUTTON"

    Then Logout