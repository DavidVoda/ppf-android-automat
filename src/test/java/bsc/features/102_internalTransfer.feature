Feature: Internal transfer

  @blocker
  Scenario: Make an internal transfer

    Given Appium server is running
    When Login is displayed

    When User is logged in

    When Dashboard is displayed
    Then Tap on "NAVBAR_PAYMENTS"
    And Validate "Payments screen" texts
    Then Tap on "INTERNAL_TRANSFER_CTA"

    When Page title is "Transfer between own accounts"
    Then Validate "Internal transfer" texts
    Then Tap on "DEBIT_ACCOUNT_PICKER"
    And Validate "Account picker screen" texts
    And Tap on "PAYMENT_ACCOUNT_1"
    Then Tap on "CREDIT_ACCOUNT_PICKER"
    And Validate "Account picker screen" texts
    And Tap on "PAYMENT_ACCOUNT_2"
    Then Enter text "5" into "PAYMENT_AMOUNT_INPUT"
    And Force close soft keyboard
    Then Tap on "PAYMENT_DETAILS_EDIT"
    And Enter text "Test payment" into "PAYMENT_DETAILS_INPUT"
    And Tap on "PAYMENT_DETAILS_SAVE_BUTTON"
    And Force close soft keyboard
    Then Tap on "PAYMENT_NEXT_BUTTON"

    When Validate "Internal transfer review" texts
    Then Tap on "PAYMENT_CONFIRM_BUTTON"

    When Page title is "Transfer successful"
    Then Validate "Internal transfer result" texts

    Then Logout

