Feature: Message create and delete

  @blocker
  Scenario: Create message from app outbox

    Given Appium server is running
    When Login is displayed

    When User is logged in

    When Tap on "NAVBAR_MESSAGES"
    When Page title "MESSAGES_TITLE" is displayed
    Then Tap on "MESSAGES_NEW_MESSAGE"
    When Page title "MESSAGES_CREATE_TITLE" is displayed
    Then Enter text "Create subject for automation" into "MESSAGES_CREATE_SUBJECT"
    Then Enter text "Create message for automation" into "MESSAGES_CREATE_BODY"
    Then Tap on "MESSAGES_SEND_BUTTON"
    Then Tap on "MESSAGES_OUTBOX"

  Scenario: Delete message from outbox

    When Page title "MESSAGES_TITLE" is displayed
    Then Tap on "MESSAGES_OUTBOX_FIRST_ITEM"
    Then Tap on "MESSAGES_DELETE_ICON"
    Then Tap on "MESSAGES_DELETE_CONFIRM"

    Then Logout