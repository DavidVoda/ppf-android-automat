Feature: Biometrics

  Scenario: Set up fingeprint easy login

    Given Appium server is running
    When User is logged in

    When Dashboard is displayed

    Then Logout
    And Go to EL setup after login
    Then Enter 4 digit passcode

    When Validate "Biometrics prompt" texts
    Then Tap on "BIOMETRICS_PROMPT_ACCEPT"
    And Touch the fingerprint sensor
    Then Validate "Notification prompt" texts
    And Tap on "NOTIFICATIONS_PROMPT_DENY"
    
    When Validate "Biometrics success" texts
    Then Tap on "BIOMETRICS_FINISH"
    And Select client

    Given Dashboard is displayed
    Then Logout

  Scenario: Easy login using fingerprint

    Given Easy login using "fingerprint"
    And Select client

    When Dashboard is displayed
    Then Logout

  Scenario: Easy login using passcode

    Given Easy login using "passcode"
    And Select client

    When Dashboard is displayed
    Then Logout















