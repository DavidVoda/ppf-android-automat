Feature: Message detail

  @blocker
  Scenario: Message detail, display incoming message

    Given Appium server is running
    When Login is displayed

    When User is logged in

    When Tap on "NAVBAR_MESSAGES"
    When Page title "MESSAGES_DETAIL_TITLE" is displayed
    #First step, select first item on the list
    Then Tap on "MESSAGES_INBOX_FIRST_ITEM"
    And Validate "Message detail" texts
    Then Tap on "BACK_BUTTON"

  Scenario: Message detail, display outgoing message

    When Tap on "NAVBAR_MESSAGES"
    When Page title "MESSAGES_OUTBOX_DETAIL_TITLE" is displayed
    #Select outbox messages
    Then Tap on "MESSAGES_OUTBOX"
    Then Tap on "MESSAGES_OUTBOX_ITEM"
    And Validate "Message detail outbox" texts
    Then Tap on "BACK_BUTTON"

    Then Logout





