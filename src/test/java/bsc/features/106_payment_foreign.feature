Feature: Payment foreign

#  Foreign payment, BBAN format + payment Sign
  Scenario: Foreign payment, BBAN format + payment Sign

    Given Appium server is running
    When Login is displayed

    When User is logged in

    When Tap on "NAVBAR_ACTION"

#   First step, debit account selection
    Then Tap on "PAYMENT_SELECT_ACCOUNT_FIRST"

#   Second step, credit account, bank and currency
    When Element "PAYMENT_TITLE" is displayed
    Then Tap on "PAYMENT_OPTION_MANUALLY"
    Then Enter text "54564654/AUDBEGCAXXX" into "PAYMENT_ACCOUNT_NUMBER_INPUT"
    Then Enter text "99.99" into "PAYMENT_AMOUNT_INPUT"
    Then Tap on "PAYMENT_CURRENCY_INPUT"
    Then Tap on "PAYMENT_CURRENCY_USD"
#    When Element "PAYMENT_PROCEED_BUTTON2" is displayed
    Then Tap on "PAYMENT_PROCEED_BUTTON"
    Then Tap on "PAYMENT_CONFIRM_BUTTON"


#   Third step, additional payment information with max values
    When Element "PAYMENT_DETAIL_TITLE" is displayed
    Then Enter text "Automation test Foreign BBAN SIGN" into "PAYMENT_PARTNER_ACCOUNT_NAME_INPUT"

    Then Enter text "Pardubicka 220" into "PAYMENT_ADDRESS1"
    Then Enter text "Prague" into "PAYMENT_ADDRESS2"
    Then Enter text "500 01" into "PAYMENT_ADDRESS3"
    Then Tap on "PAYMENT_COUNTRY"
    Then Tap on "PAYMENT_COUNTRY_CZ"

    When Swipe "UP" until "PAYMENT_MESSAGE_BENEFICIARY_INPUT" is visible
    Then Tap on "PAYMENT_CORRESPONDENT_BANK_INPUT"
    Then Tap on "PAYMENT_CORRESPONDENT_BANK_FIRST"
    Then Enter text "ABCDEFG" into "PAYMENT_MESSAGE_BENEFICIARY_INPUT"
    Then Enter text "ABCDEFG" into "PAYMENT_MY_NOTES_INPUT"
    Then Enter text "ABCDEFG" into "PAYMENT_MESSAGE_FOR_BANK"
    Then Enter text "123456" into "PAYMENT_REFERENCE"

    Then Tap on "PAYMENT_CONFIRM_BUTTON"

#   Sign payment
    When Element "PAYMENT_DETAIL_TITLE" is displayed
    Then Certificate by SMS

#   Double back to dashboard and logout
    Then Tap on "BACK_BUTTON"
    Then Tap on "BACK_BUTTON"


#  Foreign payment, BBAN format + payment Edit
  Scenario: Foreign payment, BBAN format + payment Edit

    When Tap on "NAVBAR_ACTION"

#   First step, debit account selection
    Then Tap on "PAYMENT_SELECT_ACCOUNT_FIRST"

#   Second step, credit account, bank and currency
    When Element "PAYMENT_TITLE" is displayed
    Then Tap on "PAYMENT_OPTION_MANUALLY"
    Then Tap on "PAYMENT_FOREIGN"
    Then Enter text "123123123" into "PAYMENT_ACCOUNT_NUMBER_INPUT"

    Then Tap on "PAYMENT_ACCOUNT_NUMBER_INPUT"
    Then Tap on "PAYMENT_BLANK_SPACE"
     #    CHOOSE BANK MANUALY
    Then Tap on "PAYMENT_BANK_INPUT"
    Then Tap on "PAYMENT_BANK_FIRST"

    Then Enter text "99.99" into "PAYMENT_AMOUNT_INPUT2"

    Then Tap on "PAYMENT_CURRENCY_INPUT"
    Then Tap on "PAYMENT_CURRENCY_USD"
    Then Tap on "PAYMENT_CONFIRM_BUTTON"


#   Third step, additional payment information with max values
    When Element "PAYMENT_DETAIL_TITLE" is displayed
    Then Enter text "Automation test Foreign BBAN SIGN" into "PAYMENT_PARTNER_ACCOUNT_NAME_INPUT"

    Then Enter text "Pardubicka 220" into "PAYMENT_ADDRESS1"
    Then Enter text "Prague" into "PAYMENT_ADDRESS2"
    Then Enter text "500 01" into "PAYMENT_ADDRESS3"
    Then Tap on "PAYMENT_COUNTRY"
    Then Tap on "PAYMENT_COUNTRY_CZ"

    When Swipe "UP" until "PAYMENT_MESSAGE_BENEFICIARY_INPUT" is visible
    Then Enter text "ABCDE12345ABCDE12345ABCDE12345ABCDE12345ABCDE12345ABCDE12345ABCDE12345ABCDE12345ABCDE12345ABCDE12345ABCDE12345ABCDE12345ABCDE12345ABCDE12345" into "PAYMENT_MESSAGE_BENEFICIARY_INPUT"
    Then Enter text "ABCDE12345ABCDE12345ABCDE12345ABCDE12345ABCDE12345ABCDE12345ABCDE12345" into "PAYMENT_MY_NOTES_INPUT"
    When Swipe "UP" until "PAYMENT_MESSAGE_FOR_BANK" is visible
    Then Enter text "ABCDE12345ABCDE12345ABCDE12345ABCDE12345ABCDE12345ABCDE12345ABCDE12345ABCDE12345ABCDE12345ABCDE12345ABCDE12345ABCDE12345ABCDE12345ABCDE12345" into "PAYMENT_MESSAGE_FOR_BANK"
    When Swipe "UP" until "PAYMENT_REFERENCE" is visible
    Then Enter text "123456" into "PAYMENT_REFERENCE"

    Then Tap on "PAYMENT_CONFIRM_BUTTON"

#   Edit payment
    When Element "PAYMENT_DETAIL_TITLE" is displayed
    Then Tap on "PAYMENT_EDIT_BUTTON"
    Then Enter text "Automation test FPO EDIT" into "PAYMENT_PARTNER_ACCOUNT_NAME_INPUT"
    Then Tap on "PAYMENT_CONFIRM_BUTTON"

#   Double back to dashboard and logout
    Then Tap on "BACK_BUTTON"
    Then Tap on "BACK_BUTTON"

    #  Foreign payment, BBAN format + RUB currency + OUR fee + payment delete
  Scenario: Foreign payment, BBAN format + RUB currency + OUR fee + payment delete

    When Tap on "NAVBAR_ACTION"

#   First step, debit account selection
    Then Tap on "PAYMENT_SELECT_ACCOUNT_FIRST"

#   Second step, credit account, bank and currency
    When Element "PAYMENT_TITLE" is displayed
    Then Tap on "PAYMENT_OPTION_MANUALLY"
    Then Tap on "PAYMENT_FOREIGN"
    Then Enter text "123123123" into "PAYMENT_ACCOUNT_NUMBER_INPUT"

    Then Tap on "PAYMENT_ACCOUNT_NUMBER_INPUT"
    Then Tap on "PAYMENT_BLANK_SPACE"
     #    CHOOSE BANK MANUALY
    Then Tap on "PAYMENT_BANK_INPUT"
    Then Tap on "PAYMENT_BANK_FIRST"

    Then Enter text "99.99" into "PAYMENT_AMOUNT_INPUT2"

    Then Tap on "PAYMENT_CURRENCY_INPUT"
    Then Tap on "PAYMENT_CURRENCY_RUB"

#    Change fees to OUR
    Then Tap on "PAYMENT_FEE"
    Then Tap on "PAYMENT_FEE_OUR"
    Then Tap on "PAYMENT_FEE_PROCEED"

#   Confirm pop up
    Then Tap on "PAYMENT_CONFIRM_BUTTON"
    Then Tap on "PAYMENT_PROCEED_BUTTON"

#   Third step, additional payment information with max values
    When Element "PAYMENT_DETAIL_TITLE" is displayed
    Then Enter text "Automation test Foreign BBAN SIGN" into "PAYMENT_PARTNER_ACCOUNT_NAME_INPUT"

    Then Enter text "Pardubicka 220" into "PAYMENT_ADDRESS1"
    Then Enter text "Prague" into "PAYMENT_ADDRESS2"
    Then Enter text "500 01" into "PAYMENT_ADDRESS3"
    Then Tap on "PAYMENT_COUNTRY"
    Then Tap on "PAYMENT_COUNTRY_CZ"

    When Swipe "UP" until "PAYMENT_MESSAGE_BENEFICIARY_INPUT" is visible
    Then Enter text "ABCDE12345ABCDE12345ABCDE12345ABCDE12345ABCDE12345ABCDE12345ABCDE12345ABCDE12345ABCDE12345ABCDE12345ABCDE12345ABCDE12345ABCDE12345ABCDE12345" into "PAYMENT_MESSAGE_BENEFICIARY_INPUT"
    Then Enter text "ABCDE12345ABCDE12345ABCDE12345ABCDE12345ABCDE12345ABCDE12345ABCDE12345" into "PAYMENT_MY_NOTES_INPUT"
    When Swipe "UP" until "PAYMENT_MESSAGE_FOR_BANK" is visible
    Then Enter text "ABCDE12345ABCDE12345ABCDE12345ABCDE12345ABCDE12345ABCDE12345ABCDE12345ABCDE12345ABCDE12345ABCDE12345ABCDE12345ABCDE12345ABCDE12345ABCDE12345" into "PAYMENT_MESSAGE_FOR_BANK"
    When Swipe "UP" until "PAYMENT_REFERENCE" is visible
    Then Enter text "123456" into "PAYMENT_REFERENCE"

    Then Tap on "PAYMENT_CONFIRM_BUTTON"

#   Delete payment
    When Element "PAYMENT_DETAIL_TITLE" is displayed
    Then Tap on "PAYMENT_DELETE_BUTTON"
    Then Tap on "WIDGET_CONFIRM_BUTTON"

#   Double back to dashboard and logout
    Then Tap on "BACK_BUTTON"
    Then Tap on "BACK_BUTTON"

    Then Logout