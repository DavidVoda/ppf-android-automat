package bsc.steps;

import bsc.core.Operations;
import com.codoid.products.exception.FilloException;
import cucumber.api.java.en.Given;

import static bsc.core.Operations.tapOn;
import static bsc.core.Operations.validateText;

public class MainSteps {

    @Given("Validate \"([^\"]*)\" texts")
    public static void validateInAppTexts(String page) throws Throwable {
        switch (page) {
            case "Login":
                validateText("LOGIN_LANG_SWITCH_ENG");
                validateText("LOGIN_WELCOME_TEXT_1");
                validateText("LOGIN_WELCOME_TEXT_2");
                //validateText("LOGIN_INPUT_USERNAME");
                //validateText("LOGIN_INPUT_PASSWORD");
                validateText("LOGIN_REMEMBER_ME_LABEL");
                validateText("LOGIN_BUTTON");
                validateText("LOGIN_FORGOT");
                validateText("LOGIN_RATES");
                validateText("LOGIN_SIGN_UP");
                break;
            case "2FA_LOGIN":
                validateText("2FA_HEADER");
                validateText("2FA_BUTTON_SMS");
                validateText("2FA_BUTTON_TOKEN");
                break;
            case "2FA_SMS":
                validateText("2FA_SMS_HEADER");
                validateText("2FA_SMS_SUBHEADER");
                validateText("2FA_SMS_AUTHORIZE");
                break;
            case "EL_PROMPT":
                validateText("EL_PROMPT_HEADER");
                validateText("EL_PROMPT_SUBHEADER");
                validateText("EL_PROMPT_ACCEPT");
                validateText("EL_PROMPT_DENY");
                break;
            case "CLIENT_SELECTION":
                validateText("CLIENT_HEADER");
                validateText("CLIENT_1");
                break;
            case "Mobile top up review":
                validateText("TOPUP_REVIEW_PRODUCT");
                validateText("TOPUP_REVIEW_PACKAGE");
                validateText("TOPUP_REVIEW_AMOUNT");
                break;
            case "External transfer review":
                validateText("EXTERNAL_BENEFICIARY_1");
                validateText("EXTERNAL_IBAN_1");
                validateText("EXTERNAL_AMOUNT_1");
                break;
            case "Biometrics prompt":
                validateText("BIOMETRICS_PROMPT_TEXT");
                validateText("BIOMETRICS_PROMPT_ACCEPT");
                break;
            case "Notification prompt":
                validateText("NOTIFICATIONS_PROMPT_TEXT");
                validateText("NOTIFICATIONS_PROMPT_DENY");
                break;
            case "Biometrics success":
                validateText("BIOMETRICS_SUCCESS");
                validateText("BIOMETRICS_FINISH");
                break;
            case "Message detail":
                validateText("MESSAGES_BODY");
                validateText("MESSAGES_SUBJECT");
                validateText("MESSAGES_DATE");
                validateText("MESSAGES_DETAIL_TITLE");
                break;
            case "Message detail outbox":
                validateText("MESSAGES_OUTBOX_BODY");
                validateText("MESSAGES_OUTBOX_SUBJECT");
                validateText("MESSAGES_OUTBOX_DATE");
                validateText("MESSAGES_OUTBOX_DETAIL_TITLE");
                break;
        }
    }
}