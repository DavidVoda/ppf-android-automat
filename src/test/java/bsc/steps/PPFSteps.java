package bsc.steps;

import bsc.Constants;
import bsc.core.Operations;
import bsc.core.RuntimeSetup;
import com.codoid.products.exception.FilloException;
import cucumber.api.java.en.Given;
import io.appium.java_client.android.AndroidDriver;
import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.NoSuchElementException;

import static bsc.core.Operations.*;

@SuppressWarnings("TestMethodWithIncorrectSignature")
public class PPFSteps {

    public AndroidDriver<?> driver = RuntimeSetup.driver;

    @Test
    @Given("Login is displayed")
    public void loginIsDisplayed() {
        driver.findElement(By.id("cz.bsc.ppfmobile.debug:id/welcome"));
    }

    @Given("Dashboard is displayed")
    public void dashboardIsDisplayed() throws Throwable {
        Operations.getElement("DASHBOARD_NAVBAR_SETTINGS");
    }


    @Given("User is logged in")
    public void userIsLoggedIn() throws Throwable {
        loginIsDisplayed();
        //MainSteps.validateInAppTexts("Login");
        Operations.tapOn("WELCOME_BUTTON");
        try {
            Operations.enterText(Constants.PPF_USERNAME, "LOGIN_INPUT_USERNAME");
            Operations.enterText(Constants.PPF_PASSWORD, "LOGIN_INPUT_PASSWORD");
        } catch (NoSuchElementException err) {
            Operations.enterText(Constants.PPF_USERNAME, "LOGIN_INPUT_USERNAME2");
            Operations.enterText(Constants.PPF_PASSWORD, "LOGIN_INPUT_PASSWORD2");
        }
        Operations.tapOn("LOGIN_BUTTON");

        MainSteps.validateInAppTexts("2FA_LOGIN");
        Operations.tapOn("2FA_BUTTON_SMS");

        MainSteps.validateInAppTexts("2FA_SMS");
        //Necessary wait on 2FA auth, otherwise login fails
        Thread.sleep(4000);
        Operations.tapOn("2FA_SMS_AUTHORIZE");

        MainSteps.validateInAppTexts("EL_PROMPT");
        Operations.tapOn("EL_PROMPT_DENY");

        MainSteps.validateInAppTexts("CLIENT_HEADER");
        Operations.tapOn("CLIENT_1_TEMP");

        Thread.sleep(4000);
    }

    @Given("Go to EL setup after login")
    public void navigateToELSetup() throws Throwable {
        Operations.tapOn("WELCOME_BUTTON");
        Operations.enterText(Constants.PPF_USERNAME, "LOGIN_INPUT_USERNAME");
        Operations.enterText(Constants.PPF_PASSWORD, "LOGIN_INPUT_PASSWORD");
        Operations.tapOn("LOGIN_BUTTON");

        MainSteps.validateInAppTexts("2FA_LOGIN");
        Operations.tapOn("2FA_BUTTON_SMS");

        MainSteps.validateInAppTexts("2FA_SMS");
        //Necessary wait on 2FA auth, otherwise login fails
        Thread.sleep(4000);
        Operations.tapOn("2FA_SMS_AUTHORIZE");

        MainSteps.validateInAppTexts("EL_PROMPT");
        Operations.tapOn("EL_PROMPT_ACCEPT");
    }

    @Given("Select client")
    public void selectClient() throws Throwable {
        MainSteps.validateInAppTexts("CLIENT_SELECTION");
        Operations.tapOn("CLIENT_1");
    }

    @Given("Logout")
    public static void logout() throws FilloException {
        tapOn("DASHBOARD_NAVBAR_SETTINGS");
        tapOn("SETTINGS_LOGOUT");
        tapOn("LOGOUT_CONFIRM_BUTTON");
    }

    @Given("Enter 4 digit passcode")
    public void enterPasscode() throws FilloException, InterruptedException {
        int i = 0;

        while (i < 2) {
            //enters set of 4 digits as passcode
            tapOn("PASSCODE_1");
            tapOn("PASSCODE_3");
            tapOn("PASSCODE_3");
            tapOn("PASSCODE_7");

            Thread.sleep(1500);

            i++;
        }
    }

    @Given("Enter 2FA on Auth")
    public void enter2FAAuth() throws FilloException {
        int i = 1;

        while (i <= 4) {
            String s = Integer.toString(i);
            Operations.enterText("1", "AUTH_2FA_" + s);
            i++;
        }

        Operations.tapOn("AUTH_2FA_LABEL");

        //VTB app is broken, you have to actually press the button twice
        tapOn("AUTH_2FA_CONFIRM");
    }

    @Given("Easy login using \"([^\"]*)\"")
    public void easyLogin(String method) throws FilloException, InterruptedException {
        switch (method) {
            case "fingerprint":
                Operations.tapOn("WELCOME_BUTTON");
                driver.fingerPrint(Constants.FINGETPRINT_ID);
                Thread.sleep(3000);

                break;
            case "passcode":
                Operations.tapOn("WELCOME_BUTTON");
                Operations.tapOn("SYSTEM_BIOMETRICS_CANCEL");
                Operations.tapOn("TRY_PIN");
                Operations.tapOn("PASSCODE_1");
                Operations.tapOn("PASSCODE_3");
                Operations.tapOn("PASSCODE_3");
                Operations.tapOn("PASSCODE_7");
                break;
        }
    }

    @Given("Element \"([^\"]*)\" is displayed")
    public void elementIsDisplayed(String element) throws Throwable {
        //Implicit wait because app is slow and some actions after this break the tests
        Thread.sleep(4000);
        Operations.getElement(element);
    }

    @Given("Certificate by SMS")
    public void certificationBySms() throws Throwable {
        Operations.tapOn("PAYMENT_CONFIRM_BUTTON");
        Thread.sleep(2000);
        MainSteps.validateInAppTexts("SMS_CERTIFICATION_TITLE");

        for (int i = 0; i < 8; i++) {
            Operations.tapOn("SMS_CERTIFICATION_KEYBOARD_1");
        }

        Operations.tapOn("SMS_CERTIFICATION_AUTH");
        Operations.tapOn("WIDGET_CONFIRM_BUTTON");
    }

    @Given("Express payment is after noon")
    public void expressPaymentIsAfterNoon() throws Throwable {
        //TODO implement method like getElement but something like is element displayed?
        if (isElementDisplayed("WIDGET_CONFIRM_BUTTON")) {
            Operations.tapOn("WIDGET_CONFIRM_BUTTON");
        }
    }
}