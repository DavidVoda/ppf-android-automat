package bsc;
/**
 * Constants, mainly for local test runs. For cloud runs everything is set in RuntimeSetup
 */
public class Constants {

    public static final String APP_ID = "com.icomvision.bsc";

    public static final String PLATFORM_VERSION = "10";
    public static final String DEVICE_NAME = "Testopal";
    public static final String APK_PATH = "C:/Users/Roman/Desktop/PPF-ppfPlay2Qa-1.2.0.apk";
    public static final String APPIUM_SERVER_IP = "localhost";
    public static final Integer APPIUM_SERVER_PORT = 4723;
    public static final String SCREENSHOT_PATH = "C:/Users/Roman/Desktop/";
    public static final String SCREENSHOT_NAME = "screenshoterino.png";
    public static final Integer FINGETPRINT_ID = 45146572;

    public static final long EXPLICIT_WAIT = 5000;

    public static final String PPF_USERNAME = "57238";
    public static final String PPF_PASSWORD = "Heslo123";
}
