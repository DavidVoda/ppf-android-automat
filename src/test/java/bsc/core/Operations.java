package bsc.core;

import bsc.Constants;
import com.codoid.products.exception.FilloException;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import io.appium.java_client.MobileElement;
import io.appium.java_client.TouchAction;
import io.appium.java_client.touch.LongPressOptions;
import io.appium.java_client.touch.WaitOptions;
import io.appium.java_client.touch.offset.PointOption;
import io.qameta.allure.Step;
import org.junit.Assert;
import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.Dimension;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.WebDriverException;

import java.time.Duration;

import static io.appium.java_client.touch.offset.ElementOption.element;

//TODO Add proper comments
@SuppressWarnings("TestMethodWithIncorrectSignature")
public class Operations {

    public enum Direction {
        UP,
        DOWN,
        LEFT,
        RIGHT;
    }

    /**
     * Taps on desired in-app element.
     *
     * @param elementName - Name of the element specified in [{@link bsc.Constants}
     */
    @Test
    @Step
    @Given("Tap on \"([^\"]*)\"")
    public static void tapOn(String elementName) throws FilloException {
        try {
            getElement(elementName).click();
            RuntimeSetup.logger.info("Tapping " + elementName);
        } catch (WebDriverException e) {
            int i = 0;

            while (i < 3) {
                RuntimeSetup.logger.error("Webdriver error within tapOn method - socket hang up? " + e);
                getElement(elementName).click();
                RuntimeSetup.logger.info("Tapping " + elementName);
                i = 3;
            }
        }
    }

    @Step
    @Given("Long press on \"([^\"]*)\" for \"(\\d+)\" seconds")
    public void longPress(String elementName, long duration) throws FilloException {
        Duration durationInSeconds = Duration.ofSeconds(duration);

        new TouchAction<>(RuntimeSetup.driver).longPress(LongPressOptions.longPressOptions()
                .withElement(element(getElement(elementName))).withDuration(durationInSeconds)).release().perform();
    }

    @Test
    @Step
    @Given("Enter text \"([^\"]*)\" into \"([^\"]*)\"")
    public static void enterText(String value, String elementName) throws FilloException {
        try {
            getElement(elementName).clear();
            getElement(elementName).sendKeys(value);
            RuntimeSetup.logger.info("Entering " + value + " value into " + elementName + " input field.");
        } catch (WebDriverException e) {
            RuntimeSetup.logger.error("Webdriver error within enterText method - socket hang up? " + e);
            getElement(elementName).clear();
            getElement(elementName).sendKeys(value);
            RuntimeSetup.logger.info("Entering " + value + " value into " + elementName + " input field.");
        }
    }

    public static void validateText(String elementName) throws FilloException {
        try {
            TableReader.readDB(elementName);
            String expectedText = TableReader.text;
            String actualText = getElement(elementName).getText();
            Assert.assertEquals(expectedText, actualText);
            RuntimeSetup.logger.info("The " + elementName + " text is correct - " + expectedText);
        } catch (WebDriverException e) {
            int i = 0;

            while (i < 2) {
                RuntimeSetup.logger.error("Webdriver error within validateText method - socket hang up? " + e);
                TableReader.readDB(elementName);
                String expectedText = TableReader.text;
                String actualText = getElement(elementName).getText();
                Assert.assertEquals(expectedText, actualText);
                RuntimeSetup.logger.info("The " + elementName + " text is correct - " + expectedText);
            }
        }
    }

    public static MobileElement getElement(String elementName) throws FilloException {
        TableReader.readDB(elementName);
        String elementType = TableReader.type;
        String elementLocator = TableReader.locator;
        String index;

        if (TableReader.index.isEmpty()) {
            index = "";
        } else {
            index = "[@index='" + TableReader.index + "']";
        }

        MobileElement result = null;

        try {
            switch (elementType) {
                case "id":
                    result = (MobileElement) RuntimeSetup.driver.findElement(By.id(elementLocator));
                    RuntimeSetup.logger.info("Element " + elementName + " found in source with ID " + elementLocator);
                    break;
                case "xpath":
                    result = (MobileElement) RuntimeSetup.driver.findElement(By.xpath(elementLocator));
                    RuntimeSetup.logger.info("Element " + elementName + " found in source with xpath");
                    break;
                case "class":
                    result = (MobileElement) RuntimeSetup.driver.findElement(By.xpath("//" + elementLocator + index));
                    RuntimeSetup.logger.info("Element " + elementName + " found in source with className " + elementLocator);
                    break;
            }
        } catch (WebDriverException e) {
            int i = 0;

            while (i < 3) {
                switch (elementType) {
                    case "id":
                        result = (MobileElement) RuntimeSetup.driver.findElement(By.id(elementLocator));
                        RuntimeSetup.logger.info("Element " + elementName + " found in source with ID " + elementLocator);
                        break;
                    case "xpath":
                        result = (MobileElement) RuntimeSetup.driver.findElement(By.xpath(elementLocator));
                        RuntimeSetup.logger.info("Element " + elementName + " found in source with xpath");
                        break;
                    case "class":
                        result = (MobileElement) RuntimeSetup.driver.findElement(By.xpath("//" + elementLocator + index));
                        RuntimeSetup.logger.info("Element " + elementName + " found in source with className " + elementLocator);
                        break;
                }
                i = 3;
            }
        }

        return result;   //TODO error handling
    }

    public static boolean isElementDisplayed(String elementName) throws FilloException {
        TableReader.readDB(elementName);
        String elementType = TableReader.type;
        String elementLocator = TableReader.locator;
        String index;

        if (TableReader.index.isEmpty()) {
            index = "";
        } else {
            index = "[@index='" + TableReader.index + "']";
        }

        boolean isDisplayed = false;

        try {
            switch (elementType) {
                case "id":
                    isDisplayed = !RuntimeSetup.driver.findElements(By.id(elementLocator)).isEmpty();
                    RuntimeSetup.logger.info("Element " + elementName + " is displayed in source with ID " + elementLocator);
                    break;
                case "xpath":
                    isDisplayed = !RuntimeSetup.driver.findElements(By.xpath(elementLocator)).isEmpty();
                    RuntimeSetup.logger.info("Element " + elementName + " is displayed in source with xpath");
                    break;
                case "class":
                    isDisplayed = !RuntimeSetup.driver.findElements(By.xpath("//" + elementLocator + index)).isEmpty();
                    RuntimeSetup.logger.info("Element " + elementName + " is displayed in source with className " + elementLocator);
                    break;
            }
        } catch (WebDriverException e) {
            int i = 0;

            while (i < 3) {
                switch (elementType) {
                    case "id":
                        isDisplayed = !RuntimeSetup.driver.findElements(By.id(elementLocator)).isEmpty();
                        RuntimeSetup.logger.info("Element " + elementName + " is displayed in source with ID " + elementLocator);
                        break;
                    case "xpath":
                        isDisplayed = !RuntimeSetup.driver.findElements(By.xpath(elementLocator)).isEmpty();
                        RuntimeSetup.logger.info("Element " + elementName + " is displayed in source with xpath");
                        break;
                    case "class":
                        isDisplayed = !RuntimeSetup.driver.findElements(By.xpath("//" + elementLocator + index)).isEmpty();
                        RuntimeSetup.logger.info("Element " + elementName + " is displayed in source with className " + elementLocator);
                        break;
                }
                i = 3;
            }
        }

        return isDisplayed;
    }

    //TODO improve, use more variables, probably percentages
    //TODO add screen ratios
    @Then("Scroll \"([^\"]*)\" %")
    public void scroll(Integer percentage) {
        int startY = (int) 0.8;
        int endY = startY - 100 / percentage;

        new TouchAction<>(RuntimeSetup.driver).longPress(LongPressOptions.longPressOptions()
                        .withPosition(PointOption.point(500, startY))).moveTo(PointOption.point(500, endY))
                .perform().release();
    }

    @Given("Page title is \"([^\"]*)\"")
    public void pageTitleCheck(String pageHeader) throws InterruptedException {
        //App is brutally slow, have to explicitly wait for the header or socket hang up error is triggered
        Thread.sleep(1500);
        int i = 0;

        //On faster devices the old header may be displayed for a split second after switching page, handled with a loop
        while (i < 100) {
            String text = null;
            //TODO Need to handle stale object exception somehow, thrown completely randomly. Need long-term verification if this solution works
            //TODO org.openqa.selenium.StaleElementReferenceException: The element 'By.id: com.icomvision.bsc.tbc:id/toolbarTitle' does not exist in DOM anymore
            try {
                MobileElement element = (MobileElement) RuntimeSetup.driver.findElement(By.xpath("/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.FrameLayout/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup/android.widget.FrameLayout/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup/android.widget.FrameLayout/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup/android.widget.FrameLayout/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup/android.widget.FrameLayout/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup[2]/android.view.ViewGroup[2]/android.widget.TextView"));
                text = element.getText();
                RuntimeSetup.logger.info("RETRY no " + i);
            } catch (Exception e) {
                MobileElement element = (MobileElement) RuntimeSetup.driver.findElement(By.xpath("/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.FrameLayout/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup/android.widget.FrameLayout/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup/android.widget.FrameLayout/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup[1]/android.widget.FrameLayout/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup/android.widget.FrameLayout/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup[2]/android.view.ViewGroup/android.widget.TextView"));
                text = element.getText();
                i++;
            }

            if (pageHeader.equals(text)) {
                RuntimeSetup.logger.info(text + " screen ready to use.");
                break;
            } else {
                i++;
                Thread.sleep(100);
                RuntimeSetup.logger.warn("Page header " + pageHeader + " not found, " + text + " is displayed. Retried " + i + " times.");
            }
        }
        //Header not correct for 50 retries, failing test
        if (i == 50) {
            MobileElement element = (MobileElement) RuntimeSetup.driver.findElement(By.id("com.icomvision.bsc.tbc:id/toolbarTitle"));
            String text = element.getText();
            Assert.assertEquals(pageHeader, text);
        }
    }

    @Given("Force close soft keyboard")
    public static void forceCloseKeyboard() {
        if (RuntimeSetup.driver.isKeyboardShown()) {
            RuntimeSetup.driver.hideKeyboard();
        } else {
            RuntimeSetup.logger.warn("Soft keyboard was not displayed, even though it was expected to be. Not closing it");
        }

    }

    @Given("Touch the fingerprint sensor")
    public void touchFingerprintSensor() throws InterruptedException {
        Thread.sleep(3000);
        RuntimeSetup.driver.fingerPrint(Constants.FINGETPRINT_ID);
        Thread.sleep(3000);
    }

    @Given("Swipe \"([^\"]*)\" until \"([^\"]*)\" is visible")
    public void swipeScreen(Direction dir, String element) throws InterruptedException, FilloException {
        //Animation default time:
        //Android: 300 ms
        //iOS: 200 ms
        final int ANIMATION_TIME = 1000;
        final int PRESS_TIME = 500;

        int edgeBorder = 10; //to avoid edges
        PointOption pointOptionStart, pointOptionEnd;

        //init screen variables
        Dimension dims = RuntimeSetup.driver.manage().window().getSize();

        //init start point = center of screen
        pointOptionStart = PointOption.point(dims.width / 2, dims.height / 2);

        switch (dir) {
            case DOWN:
                pointOptionEnd = PointOption.point(dims.width / 2, dims.height - edgeBorder);
                break;
            case UP:
                pointOptionEnd = PointOption.point(dims.width / 2, edgeBorder);
                break;
            case LEFT:
                pointOptionEnd = PointOption.point(edgeBorder, dims.height / 2);
                break;
            case RIGHT:
                pointOptionEnd = PointOption.point(dims.width - edgeBorder, dims.height / 2);
                break;
            default:
                throw new IllegalArgumentException("swipeScreen(): dir: '" + dir + "' NOT supported");
        }

        int i = 0;

        while (i < 10) {

            try {
                RuntimeSetup.logger.debug("is displayed? " + getElement(element).isDisplayed());
                getElement(element).isDisplayed();
                i = 10;
            } catch (NoSuchElementException err) {
                try {
                    new TouchAction(RuntimeSetup.driver)
                            .press(pointOptionStart)
                            .waitAction(WaitOptions.waitOptions(Duration.ofMillis(PRESS_TIME)))
                            .moveTo(pointOptionEnd)
                            .release().perform();
                    i++;
                } catch (Exception e) {
                    System.err.println("swipeScreen(): TouchAction FAILED\n" + e.getMessage());
                    return;
                }
                Thread.sleep(ANIMATION_TIME);
            }
        }
    }
}