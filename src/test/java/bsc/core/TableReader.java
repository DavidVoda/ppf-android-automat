package bsc.core;

import com.codoid.products.exception.FilloException;
import com.codoid.products.fillo.Connection;
import com.codoid.products.fillo.Fillo;
import com.codoid.products.fillo.Recordset;
import org.openqa.selenium.WebDriverException;

import java.io.IOException;
import java.net.URL;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.nio.file.StandardCopyOption;

public class TableReader {

    //table/db rows
    public static String id;
    public static String locator;
    public static String type;
    public static String index;
    public static String text;

    private static final String DATA_LOCATION = "table\\Book.xlsx";

    //Download and store the data table
    public static String googleDriveLink = "https://drive.google.com/uc?export=download&id=1yy32PfgMu8bpYF6MDeaIYuBZIS0nyvsa";

    public static void fetchTable() throws IOException {
        Files.copy(new URL(googleDriveLink).openStream(), Paths.get(DATA_LOCATION), StandardCopyOption.REPLACE_EXISTING);
    }

    public static void readDB(String elementName) throws FilloException {

        Fillo fillo = new Fillo();

        //Instantiate a DB from our data xls
        Connection connection = fillo.getConnection(DATA_LOCATION);

        //SQL query
        String strQuery = "Select * from Sheet1 where uniqueID='" + elementName + "'";
        RuntimeSetup.logger.info("SQL Query: "+strQuery);
        Recordset recordset = connection.executeQuery(strQuery);

        //Parse the results
        while (recordset.next()) {
            id = recordset.getField("uniqueID");
            index = recordset.getField("index");
            text = recordset.getField("text");

            type = recordset.getField("id type android");
            locator = recordset.getField("locator android");
        }

        //Close connection to avoid memory leak
        recordset.close();
        connection.close();
    }
}
