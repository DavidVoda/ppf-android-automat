package bsc.core;

import bsc.Constants;
import bsc.steps.PPFSteps;
import cucumber.api.Scenario;
import cucumber.api.java.After;
import cucumber.api.java.Before;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import io.appium.java_client.android.AndroidDriver;
import io.qameta.allure.Allure;
import org.apache.commons.io.FileUtils;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.File;
import java.net.Socket;
import java.net.URL;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.concurrent.TimeUnit;

import static bsc.Constants.*;

public class RuntimeSetup {

    public static AndroidDriver<?> driver = null;
    public static final Logger logger = LoggerFactory.getLogger(RuntimeSetup.class);

    /**
     * Checks if appium server is currently being used.
     *
     * @param host Server address
     * @param port Server port
     * @return true if running, false otherwise
     */
    public static boolean getServerListener(String host, int port) {
        Socket socket = null;
        try {
            socket = new Socket(host, port);
            return false;
        } catch (Exception e) {
            logger.error("Socket init exception", e);
            return false;
        } finally {
            if (socket != null) {
                try {
                    socket.close();
                } catch (Exception e) {
                    logger.error("Socket close exception", e);
                }
            }
        }
    }

    /**
     * Checks app is open by looking into page source, finding package name
     *
     * @return true if running
     */
    @SuppressWarnings("ResultOfMethodCallIgnored")
    public static boolean isAppRunning() {
        try {
            driver.getPageSource().contains(APP_ID);
            return true;
        } catch (Exception e) {
            return false;
        }
    }

    public void startServer() throws Throwable {

        try {
            TableReader.fetchTable();
        } catch (Exception e) {
            System.out.println("Failed to get the table from cloud storage "+e);
        }

        DesiredCapabilities caps = new DesiredCapabilities();

        //for running on locale
        caps.setCapability("platformName", "Android");
        caps.setCapability("automationName", "Appium");
        caps.setCapability("platformVersion", PLATFORM_VERSION);
        caps.setCapability("deviceName", DEVICE_NAME);
        caps.setCapability("avd", DEVICE_NAME);
        caps.setCapability("app", APK_PATH);
        caps.setCapability("isHeadless", "false");
        caps.setCapability("noReset", "true");
        caps.setCapability("takes_screenshot", "true");
        caps.setCapability("avd", DEVICE_NAME);
        //caps.setCapability("appWaitActivity", "cz.bsc.mb.activities.WelcomeActivity, cz.bsc.mb.activities.LoginActivity");
        URL remoteUrl = new URL("http://localhost:4723/wd/hub");

        //for running on farm
/*        DateTimeFormatter dtf = DateTimeFormatter.ofPattern("yyyy/dd/MM HH:mm:ss");
        LocalDateTime now = LocalDateTime.now();

        caps.setCapability("platformVersion", "10");
        caps.setCapability("bitbar_device", "Google Pixel 2");
        caps.setCapability("bitbar_app", "135155006");
        caps.setCapability("platformName", "Android");
        caps.setCapability("bitbar_apiKey", "SPE1CvtoSZ0G6rZzFELn1oKWKc0EkRej");
        caps.setCapability("bitbar_findDevice", true);
        caps.setCapability("bitbar_project", "automatTest");
        caps.setCapability("bitbar_testrun", "testopal " + dtf.format(now));
        caps.setCapability("appWaitActivity", "cz.bsc.mb.activities.WelcomeActivity, cz.bsc.mb.activities.LoginActivity");
        caps.setCapability("noReset", true);
        caps.setCapability("takes_screenshot", "true");
        URL remoteUrl = new URL("https://appium.bitbar.com/wd/hub");*/

        driver = new AndroidDriver<>(remoteUrl, caps);
        driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
    }

    public static boolean isLastScenario;

    //Checks if the current scenario is the final one, if so ends the Bitbar cloud session
    @Before
    public void handleBrokenSession(Scenario scenario) {
        String scenarioName = scenario.getName();
        System.out.println("Current scenario is " + scenarioName);

        if (scenarioName.equals("End the appium server session on Bitbar")) {
            driver.quit();
            logger.warn("Killing bitbar session");
            isLastScenario=true;
        } else {
            isLastScenario=false;
        }
    }

    /*
     * Checks if app is running. If it is, continues with test. If not, checks if
     * appium server is running. If it is, restarts. If it isn't, starts it.
     */
    @Given("^Appium server is running$")
    public void appiumServerIsRunning() throws Throwable {
        if (isLastScenario) {
            driver.quit();
            logger.warn("Killing bitbar session");
        } else {
            int i = 0;
            while (i < 2) {
                if (isAppRunning()) {
                        Operations.validateText("WELCOME_BUTTON");
                    logger.info("App is running, language set to english, proceeding with the test.");
                    i = 5;

                } else {
                    logger.info("App is not running, starting the server");

                    startServer();
                    i++;
                }
            }
        }
    }

    /**
     * Pauses the test for x seconds. Used as a precaution in places where we expect a long loading which might break the scenario.
     */
    @Then("^Wait a bit$")
    public void waitABit() throws Throwable {
        Thread.sleep(Constants.EXPLICIT_WAIT);
    }

    /**
     * Method used when scenario fails.
     */
    @After
    public void tearDown(final Scenario scenario) {
        logger.info("End of " + scenario.getName());

        try {
            if (scenario.isFailed()) {
                File scrFile = ((TakesScreenshot) driver).getScreenshotAs(OutputType.FILE);
                FileUtils.copyFile(scrFile, new File(SCREENSHOT_PATH + SCREENSHOT_NAME));

                Path content = Paths.get(SCREENSHOT_PATH + SCREENSHOT_NAME);
                logger.info(content.toString());
                logger.info(scenario.getId());
                Allure.addAttachment(scenario.getId(), Files.newInputStream(content));

                logger.warn("Scenario failed, re-installing the application!");
                driver.resetApp();
                Thread.sleep(5000);
                logger.info("App re-installed and ready for next usage.");

                Thread.sleep(5000);
            }
        } catch (final Exception e) {
            logger.error("Tear down error", e);
        }
    }

    @Then("^End appium session$")
    public void endAppiumSession() {
        logger.warn("Session ended");
    }
}

